json.extract! note, :id, :note_name, :note_entry, :created_at, :updated_at
json.url note_url(note, format: :json)
