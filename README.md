# QuickNotes Application

## Description

Basic notes application built with Ruby on Rails, PostgreSQL and Bootstrap.

## Prerequisites 

* Ruby 2.7.0
* Rails
* PostgreSQL
* Bootstrap installed and confiured with Ruby and Rails.

[Click here](/ruby-rails-psql-install-fedora-33.MD/) to view the installation guide to install and configure Ruby on Rails with PostgreSQL on Linux.

## Installation

### Clone the repository

<code>git clone https://gitlab.com/blitziggy/ruby-note-app.git  

### Start the application

Run the following commands in your terminal

* cd ruby-note-app
* sudo systemctl start postgresql-12 - Make sure PostgreSQL service is running.
* rails server

## Run the application

* Browse to http://localhost:3000 

### Homepage

* The homepage of the QuickNotes application.

![homenote](images/homenote.png)



### Create a new Note

* To create a new note click on the "Create new Note" button in the top right of the homepage.

![newnote](images/newnote.png)



### Edit a Note

* To edit a note, click on the "Edit" button of the note. This will take you to the "Editing Note" page.

![editnote](images/editnote1.png)

![editnote](images/editnote2.png)



### Delete a Note

* To delete a note, click on the "Delete" button on the note.

![deletenote](images/deletenote.png)




